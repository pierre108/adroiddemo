Feature: DataSet Copy operations
  @automated-rest @UI @DEV @FDM @SysMan
  Scenario: Copy DataSet without submit
    Given exist approved DataSet
    And user with profile "fdm" is logged
    When DataSet copying start
    And select one existing DataSet version of "0406_BG"
    And the user fill up all information for destination Database
      |PREFIX| DATABASE_TYPE| ENVIRONMENT | SYSTEM_INSTANCE| DATABASE_NAME |
      |T| hadoop| qa | bi-as-datalake-qa-us | dvds_master_ca_current |
    And copy of DataSet is saved
    Then new DataSet is visible in the list
    And new DataSet has Version new

  @automated-rest @UI @DEV @FDM @SysMan
  Scenario: Copy DataSet with submit
    Given exist approved DataSet
    And user with profile "fdm" is logged
    When DataSet copying start
    And select one existing DataSet version of "0406_BG"
    And the user fill up all information for destination Database
      |PREFIX| DATABASE_TYPE| ENVIRONMENT | SYSTEM_INSTANCE| DATABASE_NAME |
      |T| hadoop| prod | bi-as-datalake-prod-us | dvds_master_ca_history|
    And copy of DataSet is saved and submitted
    Then new DataSet is visible in the list
    And new DataSet has Version approved

  @automated-rest @UI @DEV @FDM @SysMan
  Scenario: Copy DataSet with business tags modification without submit
    Given exist approved DataSet
    And user with profile "fdm" is logged
    When DataSet copying start
    And select one existing DataSet version of "0406_BG_CopyDataSetDisapproved"
    And save for late check current values
    And the user fill up all information for destination Database
      |PREFIX| DATABASE_TYPE| ENVIRONMENT | SYSTEM_INSTANCE| DATABASE_NAME |
      |T| hadoop| prod | bi-as-datalake-prod-us | dvds_master_ca_history|
    And the user add following tags
      | TAG |
      | newBusinessTags |
      | copyDataset |
    And copy of DataSet is saved
    Then new DataSet is visible in the list
    And new DataSet has Version new
    And new DataSet have values same as original DataSet except business tags
    And new DataSet has only new business tags

  @automated-rest @UI @DEV @FDM @SysMan
  Scenario: Copy DataSet with owner modification without submit
    Given exist approved DataSet
    And user with profile "fdm" is logged
    When DataSet copying start
    And select one existing DataSet version of "Account"
    And save for late check current values
    And the user fill up all information for destination Database
      |PREFIX| DATABASE_TYPE| ENVIRONMENT | SYSTEM_INSTANCE| DATABASE_NAME |
      |T| hadoop| prod | bi-as-datalake-prod-us | dvds_master_ca_history|
    And the user add owner "x2dlmsdowner"
    And copy of DataSet is saved
    Then new DataSet is visible in the list
    And new DataSet has Version new
    And new DataSet has only new owner
    And new DataSet have values same as original DataSet except owner

  @automated-rest @DEV @FDM @SysMan
  Scenario: Copy DataSet to No Hadoop without submit
    Given exist approved DataSet
    And user with profile "fdm" is logged
    When DataSet copying start
    And select one existing DataSet version of "0406_BG"
    And the user fill up all information for destination Database
      |PREFIX| DATABASE_TYPE| ENVIRONMENT | SYSTEM_INSTANCE| DATABASE_NAME |
      |T| oracle| prod | bi-as-odldsa-prod | test_dlms |
    And copy of DataSet is saved
    Then new DataSet is visible in the list
    And new DataSet has Version new

  @automated-rest @DEV @FDM @SysMan
  Scenario: Copy DataSet No Hadoop with submit
    Given exist approved DataSet
    And user with profile "fdm" is logged
    When DataSet copying start
    And select one existing DataSet version of "0406_BG"
    And the user fill up all information for destination Database
      |PREFIX| DATABASE_TYPE| ENVIRONMENT | SYSTEM_INSTANCE| DATABASE_NAME |
      |T| oracle| prod | bi-as-odldsa-prod | test_dlms |
    And copy of DataSet is saved and submitted
    Then new DataSet is visible in the list
    And new DataSet has Version approved


  @automated-rest @positive @DEV @FDM @SysMan
  Scenario: Copy DataSet with PARQUET format into Hive Database without submit
    Given exist approved DataSet
    And user with profile "fdm" is logged
    When DataSet copying start
    And select one existing DataSet version of "0406_Customer_Attribute_BI__c"
    And the user fill up all information for destination Database
      |PREFIX| DATABASE_TYPE| ENVIRONMENT | SYSTEM_INSTANCE| DATABASE_NAME |
      |T| hadoop| prod | bi-as-datalake-prod-us | dvds_master_ca_history|
    And copy of DataSet is saved
    Then new DataSet is visible in the list
    And new DataSet has Version new

  @automated-rest @positive @DEV @FDM @SysMan
  Scenario: Copy DataSet with PARQUET format into Hive Database with submit
    Given exist approved DataSet
    And user with profile "fdm" is logged
    When DataSet copying start
    And select one existing DataSet version of "0406_Customer_Attribute_BI__c"
    And the user fill up all information for destination Database
      |PREFIX| DATABASE_TYPE| ENVIRONMENT | SYSTEM_INSTANCE| DATABASE_NAME |
      |T| hadoop| prod | bi-as-datalake-prod-us | dvds_master_ca_history|
    And copy of DataSet is saved and submitted
    Then new DataSet is visible in the list
    And new DataSet has Version approved

  @automated-rest @positive @DEV @hive @FDM @SysMan
  Scenario: Copy DataSet with PARQUET format into Hive Database with submit
    Given exist approved DataSet
    And user with profile "fdm" is logged
    When DataSet copying start
    And select one existing DataSet version of "test_PETER"
    And the user fill up all information for destination Database
      |PREFIX| DATABASE_TYPE| ENVIRONMENT | SYSTEM_INSTANCE| DATABASE_NAME |
      |T| hadoop| prod | bi-as-datalake-prod-us | dvds_master_ca_history|
    And copy of DataSet is saved and submitted
    Then new DataSet is visible in the list
    And new DataSet has Version approved
    And Hive table is created with corresponding name
    And Hive table has correct structure
      | boolean | string | int |
      |  0      | 2      |0    |

  @automated-rest @positive @Oracle
  Scenario: Copy DataSet with PARQUET format into Oracle Database with submit
    Given exist approved DataSet
    And user with profile "fdm" is logged
    When DataSet copying start
    And select one existing DataSet version of "test_PETER"
    And the user fill up all information for destination Database
      |PREFIX| DATABASE_TYPE| ENVIRONMENT | SYSTEM_INSTANCE| DATABASE_NAME |
      |T| oracle| prod | BI-AS-ODLDSA-PROD | ORACLE_DLMS_TEST |
    And the user add following tags
      | TAG  |
      | tag1 |
      | tag2 |
      | tag3 |
    And copy of DataSet is saved and submitted
    Then new DataSet is visible in the list
    And new DataSet has Version approved
    And DataSet has all business tags

  @automated-rest @positive @QA @hive @FDM @SysMan
  Scenario: Copy DataSet with PARQUET format into Hive Database with submit
    Given exist approved DataSet
    And user with profile "fdm" is logged
    When DataSet copying start
    And select one existing DataSet version of "PeterTest3"
    And the user fill up all information for destination Database
      |PREFIX| DATABASE_TYPE| ENVIRONMENT | SYSTEM_INSTANCE| DATABASE_NAME |
      |T| hadoop| prod | bi-as-datalake-prod-us | dvds_master_ca_history|
    And copy of DataSet is saved and submitted
    Then new DataSet is visible in the list
    And new DataSet has Version approved
    And Hive table is created with corresponding name
    And Hive table has correct structure
      | boolean | string | int |
      |  0      | 1      |0    |


  @automated-rest @positive @QA @FDM @SysMan
  Scenario: Copy DataSet with PARQUET format into Hive Database with submit
    Given exist approved DataSet
    And user with profile "fdm" is logged
    When DataSet copying start
    And select one existing DataSet version of "PeterTest3"
    And the user fill up all information for destination Database
      |PREFIX| DATABASE_TYPE| ENVIRONMENT | SYSTEM_INSTANCE| DATABASE_NAME |
      |T| hadoop| prod | bi-as-datalake-prod-us | country_stats_master_nr |
    And copy of DataSet is saved
    Then new DataSet is visible in the list
    And new DataSet has Version new

  @automated-rest @positive @QA @FDM @SysMan
  Scenario: Copy DataSet with PARQUET format into Hive Database with submit
    Given exist approved DataSet
    And user with profile "fdm" is logged
    When DataSet copying start
    And select one existing DataSet version of "testOracle"
    And the user fill up all information for destination Database
      |PREFIX| DATABASE_TYPE| ENVIRONMENT | SYSTEM_INSTANCE| DATABASE_NAME |
      |T| oracle| prod | BI-AS-ODLDSA-PROD | TEST_DB |
    And copy of DataSet is saved and submitted
    Then new DataSet is visible in the list
    And new DataSet has Version approved
